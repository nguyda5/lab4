package geometry;

public class Circle implements Shape{
	private double radius;
	
	public Circle(int givenRadius) {
		this.radius = givenRadius;
	}

	public double getRadius() {
		return this.radius;
	}
	
	public double getArea() {
		double area = Math.PI*radius*radius;
		return area;
	}
	
	public double getPerimeter() {
		double perimeter = 2*Math.PI*radius;
		return perimeter;
	}
}
