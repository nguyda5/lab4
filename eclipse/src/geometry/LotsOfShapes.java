package geometry;

public class LotsOfShapes {
	public static void main(String[]args) {
		Shape[] shapeList = new Shape[5];
		shapeList[0] = new Rectangle(5,6);
		shapeList[1] = new Rectangle(2,10);
		shapeList[2] = new Circle(6);
		shapeList[3] = new Circle(8);
		shapeList[4] = new Square(9);
		
		for(int i = 0; i < shapeList.length; i++) {
			System.out.println("Shape " + i);
			System.out.println("Area: " + shapeList[i].getArea());
			System.out.println("Perimeter: " + shapeList[i].getPerimeter());
		}
	}
}
