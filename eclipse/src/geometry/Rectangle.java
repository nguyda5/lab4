package geometry;

public class Rectangle implements Shape {
	private double length;
	private double width;
	
	public Rectangle(double givenLength, double givenWidth) {
		this.length = givenLength;
		this.width = givenWidth;
	}
	
	public double getLength() {
		return this.length;
	}
	
	public double getWidth() {
		return this.width;
	}
	
	public double getArea() {
		double area = this.length*this.width;
		return area;
	}
	
	public double getPerimeter() {
		double perimeter = this.length*2+this.width*2;
		return perimeter;
	}
}
