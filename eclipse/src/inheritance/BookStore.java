package inheritance;

public class BookStore {

	public static void main(String[] args) {
		Book[] bookArr = new Book[5];
		bookArr[0] = new Book("About Dragons","David");
		bookArr[1] = new ElectronicBooks("How to cheat in school","Anonymous",5);
		bookArr[2] = new ElectronicBooks("Cooking 101","Gordon Ramsay",10);
		bookArr[3] = new Book("Horror Stories","Alex");
		bookArr[4] = new ElectronicBooks("Working out 101","Daniel",20);

		for(int i = 0; i < bookArr.length; i++) {
			System.out.println(bookArr[i].toString());
		}
	}

}
