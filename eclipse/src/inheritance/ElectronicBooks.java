package inheritance;

public class ElectronicBooks extends Book{
	private int numberBytes;
	
	public ElectronicBooks(String givenTitle,String givenAuthor,int givenBytes) {
		super(givenTitle, givenAuthor);
		this.numberBytes = givenBytes;
	}
	
	public int getNumberBytes(){
		return this.numberBytes;
	}
	
	public String toString() {
		return (super.toString() + " Bytes: " + this.numberBytes);
	}
}
