package inheritance;

public class Book {
	protected String title;
	private String author;
	
	public Book(String givenTitle, String givenAuthor) {
		this.title = givenTitle;
		this.author = givenAuthor;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public String getAuthor() {
		return this.author;
	}
	public String toString() {
		return ("Title: " + this.title + " Author: " + this.author);
	}
}
